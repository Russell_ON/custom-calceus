<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatesShoesSizesPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoes_sizes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shoes_id')->constrained();
            $table->foreignId('sizes_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoes_sizes', function(Blueprint $table){
            $table->dropForeign(['shoes_id']);
            $table->dropColumn('shoes_id');

            $table->dropForeign(['sizes_id']);
            $table->dropColumn('sizes_id');
        });
    }
}
