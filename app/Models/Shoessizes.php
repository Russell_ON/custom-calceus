<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shoessizes extends Model
{
    use HasFactory;

    protected $table = 'shoes_sizes';

    protected $fillable = ['shoes_id', 'sizes_id', 'stock'];
}
