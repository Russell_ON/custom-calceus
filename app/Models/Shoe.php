<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shoe extends Model
{
    use HasFactory;

    protected $fillable = ['price', 'title', 'brand_id', 'image'];

    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function sizes(){
        return $this->hasMany(Size::class);
    }

    public function images(){
        return $this->hasMany(Image::class);
    }
}
