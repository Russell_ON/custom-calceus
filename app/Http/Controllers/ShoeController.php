<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Image;
use App\Models\Shoe;
use App\Models\Shoessizes;
use App\Models\Size;
use Illuminate\Http\Request;

class ShoeController extends Controller
{
    public function index(){

        $brands = Brand::get();
        $shoes = Shoe::get();

        // dd($brands);
        return view('collection.index', [
            'shoes' => $shoes,
            'brands' => $brands,
        ]);
    }

    public function show($id){

        $shoesSize = Shoessizes::where('shoes_id', $id)->get();

        $sizes_shoes = Shoe::find($id)->sizes()->get();
        dd($sizes_shoes);

        // dd($shoesSize);

        $images = Image::where('shoe_id', $id)->get();

        $sizes = Size::get();


        $shoe = Shoe::findOrFail($id);

        return view('collection.show',[
            'shoe' => $shoe,
            'images' => $images,
            'sizes' => $sizes,
        ]);
    }

    public function store(Request $request){


        //validation for form
        $this->validate($request, [
            'title' => 'required|max:255',
            'price' => 'required|numeric',
            'brand_id' => 'required',
        ]);

        //validation for image
        if($request->hasFile('image')){
            $this->validate($request,[
                'image' => 'required|file|image',
            ]);
        }

        $imagePath = $request->image->store('uploads', 'public');

        Shoe::create([
            'title' => $request->title,
            'price' => $request->price,
            'brand_id' => $request->brand_id,
            'image' => $imagePath,
        ]);

        return redirect()->route('collection');
    }


}
