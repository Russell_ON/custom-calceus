<?php

namespace App\Http\Controllers;

use App\Models\Shoessizes;
use App\Models\Size;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function store($shoe, Request $request){

        $this->validate($request,[
            'stock' => 'required|integer',
        ]);

        $shoesSize = Shoessizes::where('shoes_id', $shoe)->where('sizes_id', $request->size)->first();

        //check if record not exists in the database if it not exists create a new record
        if(! $shoesSize){
            Shoessizes::create([
                'shoes_id' => $shoe,
                'sizes_id' => $request->size,
                'stock' => $request->stock,
            ]);
        }else{
            //if record exists add request stock to current stock
            $shoesSize->stock = $shoesSize->stock+$request->stock;

            $shoesSize->save();
        }

        return redirect()->back();
    }
}
