<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function store($shoe, Request $request){

        if($request->hasFile('image')){
            $this->validate($request,[
                'image' => 'required|image|file'
            ]);

            $imagePath = $request->image->store('uploads', 'public');

            Image::create([
                'shoe_id' => $shoe,
                'image' => $imagePath
            ]);

        }

        return redirect()->back();
    }
}
