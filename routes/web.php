<?php

use App\Http\Controllers\auth\LogoutController;
use App\Http\Controllers\CreateController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\ShoeController;
use App\Http\Controllers\SizeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/collection', [ShoeController::class, 'index'])->name('collection');
Route::post('/collection', [ShoeController::class, 'store'])->name('collection.store')->middleware(['auth']);
Route::post('/shoe/{shoe}', [ImageController::class, 'store'])->name('image.store')->middleware(['auth']);
Route::post('/shoe/{shoe}/size', [SizeController::class, 'store'])->name('size.store')->middleware(['auth']);
Route::get('/shoe/{shoe:title}', [ShoeController::class, 'show'])->name('details');
Route::post('/logout', [LogoutController::class, 'destroy'])->name('logout');
Route::get('/create-your-own', [CreateController::class, 'index'])->name('create-your-own');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
