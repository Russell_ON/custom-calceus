const nav = document.querySelector(".main-nav");
const toggleBtn = document.querySelector(".toggle-btn");


toggleBtn.addEventListener("click", function(){
    if(!nav.classList.contains("mobile-nav")){
        nav.classList.add("mobile-nav")
    }else{
        nav.classList.remove("mobile-nav")
    }
})



window.addEventListener("scroll", function(){
    var y = window.scrollY
    if(y > 10){
        nav.classList.add("scroll-nav")
    }else{
        nav.classList.remove("scroll-nav")
    }

})
