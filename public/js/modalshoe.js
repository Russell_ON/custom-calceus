const btnShoe = document.getElementById("open-modal-shoe")
const modalShoe = document.getElementById("modal-shoe")
const containerShoe = document.getElementById("modal-container-shoe")


btnShoe.addEventListener("click", function(){
    modalShoe.style.display = "block";
    containerShoe.style.display = "block"
})

modalShoe.addEventListener("click", function(){
    containerShoe.style.display = "none"
    modalShoe.style.display = "none"
})
