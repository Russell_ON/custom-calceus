const quantityBtns = document.querySelectorAll(".btn");
const decrementBtn = document.querySelector(".decrement-quantity")
let currentValue = document.querySelector(".quantity-value");


quantityBtns.forEach(function(btn){
    btn.addEventListener("click", function(e){
        const btn = e.currentTarget.classList;

        if(btn.contains("increment-quantity")){
            currentValue.value++
        }else if(btn.contains("decrement-quantity")){
            currentValue.value--
        }

        if(currentValue.value < 1){
            currentValue.value = 1;
        }else if(currentValue.value > 100){
            currentValue.value = 100
        }

    })
})

currentValue.addEventListener("keydown", function(e){
    currentValue.value = e.target.value
})


