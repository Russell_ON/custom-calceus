const alternateNav = document.querySelector(".alternate-nav");
const toggleBtn = document.querySelector(".toggle-btn");


toggleBtn.addEventListener("click", function(){
    if(!alternateNav.classList.contains("mobile-nav")){
        alternateNav.classList.add("mobile-nav")
    }else{
        alternateNav.classList.remove("mobile-nav")
    }
})
