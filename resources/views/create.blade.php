<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <style>
            body {
                font-family: 'Helvetica';
            }
        </style>
    </head>

    <body>
        <x-headeretc/>
        <main id="create">

            <div class="form-container">
                <div class="inner-container">
                    <div class="inner-container-header">
                        <h1>Ontwerp verzoek</h1>
                    </div>
                    <form action="">
                        <div class="form-input">
                            <label for="firstname">Voornaam</label>
                            <input type="text" id="firstname" name="firstname" placeholder="Type je voornaam...">
                        </div>
                        <div class="form-input">
                            <label for="lastname">Achternaam</label>
                            <input type="text" id="lastname" name="lastname" placeholder="Type je achternaam...">
                        </div>
                        <div class="form-input">
                            <label for="email">Email</label>
                            <input type="text" id="email" name="email" placeholder="Type je email">
                        </div>
                        <div class="form-input">
                            <label for="gender">Geslacht</label>
                            <input type="text" id="email" name="email" placeholder="Type je geslacht">
                        </div>
                        <div class="form-input">
                            <label for="design">Design</label>
                            <textarea name="design" id="design" cols="30" rows="5"></textarea>
                        </div>

                        <button type="submit">Versturen</button>
                    </form>
                </div>
            </div>
        </main>
        <x-footer/>
        <script src="/js/headeralt.js"></script>
    </body>
