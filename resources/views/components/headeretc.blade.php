<header class="alternate-nav">
    <nav>
        <div>
            <div class="nav-header">
                <div class="header-logo">
                    <a href="{{ route('home') }}">
                        <img class="logo" src="{{ asset('images/logo.svg') }}" alt="">
                    </a>
                </div>
                <div class="toggle-button">
                    <button class="toggle-btn">
                        <img src="{{ asset('images/icons/hamburger.svg') }}" alt="">
                    </button>
                </div>
            </div>
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('collection') }}">Collection</a></li>
                <li><a href="{{ route('create-your-own') }}">Create your own</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Size scale</a></li>
                <li><a id="sale" href="#">SALE!</a></li>
                @auth
                    <form class="nav-logout" action="{{ route('logout') }}" method="POST">
                        @csrf
                        <li>
                            <button type="submit">
                                <i class="fas fa-sign-out-alt"></i>
                            </button>
                        </li>
                    </form>
                @endauth
            </ul>
        </div>

    </nav>
</header>
