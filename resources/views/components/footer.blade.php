<footer>
    <section>
        <h4>INFORMATION</h4>
        <p>Help</p>
        <p>Track and Trace</p>
        <p>Delivery and return</p>
        <p>2x Vip delivery</p>
        <p>5% sale for students</p>
    </section>
    <section>
        <h4>Custom Calceus</h4>
        <p>About us</p>
        <p>Wanna work here? oof.</p>
        <p>Website for investors</p>

        <section>
            <h4>More about Calceus</h4>
            <p>Calceus App for mobile</p>
            <p>Calceus marketplace</p>
            <p>Giftcard</p>
        </section>
    </section>
    <section>
        <h4>SHOPS IN:</h4>
        <p>Amsterdam, The Netherlands</p>
        <p>New York, USA</p>
        <p>Los Angeles, USA</p>
        <p>Accra, Ghana</p>
    </section>
    <section>
        <h4>FOLLOW/CONTACT US ON:</h4>
        <ul>
            <li><img src="{{ asset('images/icons/twitter.svg') }}" alt=""></li>
            <li><img src="{{ asset('images/icons/instagram.svg') }}" alt=""></li>
            <li><img src="{{ asset('images/icons/pinterest.svg') }}" alt=""></li>
            <li><img src="{{ asset('images/icons/facebook.svg') }}" alt=""></li>
        </ul>
    </section>
</footer>
