<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        {{-- Script --}}


        <style>
            body {
                font-family: 'Helvetica';
            }
        </style>
    </head>
    <body>
        <x-headeretc/>

        <div id="modal"></div>
        <div id="modal-container">
            <form action="{{ route('image.store', ['shoe' => $shoe->id]) }}" method="POST" enctype="multipart/form-data">

                @csrf
                <div class="form-group">
                    <label for="image">Afbeelding</label>
                    <input class="form-shoes" type="file" name="image" id="image">
                    @error('image')
                    <span class="error">{{ $message }}</span>
                @enderror
                </div>
                <button type="submit">Toevoegen</button>
            </form>
        </div>

        <div id="modal-shoe"></div>
        <div id="modal-container-shoe">
            <h2>Voeg maat toe voor <span class="shoe-modal-title">{{ $shoe->title }}</span></h2>
            <form action="{{ route('size.store', ['shoe' => $shoe->id]) }}" method="POST">

                @csrf

                <label for="size">Kies een maat:</label>
                <select class="size" name="size">
                    @foreach ($sizes as $size )
                        <option value="{{ $size->id }}">{{ $size->number }}</option>
                    @endforeach
                </select>

                <div>
                    <label for="Stock">Stock</label>
                    <input class="number" type="number" id="stock" name="stock" min="1" max="100">
                </div>

                <button class="shoe-submit" type="submit">Voeg toe</button>

            </form>
        </div>

        <main id="details">
            <section>
                    <img class="gallery" src="/storage/{{ $shoe->image }}" alt="">
                @foreach ($images as $image )
                    <img class="gallery" src="/storage/{{ $image->image }}" alt="">
                @endforeach


            </section>
            <section>
                <div>
                    <img class="main" src="/storage/{{$shoe->image}}" alt="">
                </div>
            </section>
            <section>
                <h1>{{ $shoe->title }}</h1>
                @auth
                    <button id="open-modal-shoe"> Voeg een maat toe</button>
                @endauth
                <h2>€{{ $shoe->price }}</h2>
                <select name="size">
                    <option value="0">Selecteer Maat</option>
                    @foreach ($sizes as $size )
                        <option value="{{ $size->id }}">{{ $size->number }}</option>
                    @endforeach
                </select>
                <div>
                    <div class="quantity">
                        <input class="quantity-value" data-min="1" data-max="100" value="1" type="text" name="quantity" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))">
                        <div class="quantity-selector">
                            <button type="button" class="btn increment-quantity"><span>+</span></button>
                            <button type="button" class="btn decrement-quantity"><span>-</span></button>
                        </div>
                    </div>
                    <button type="submit">Toevoegen aan winkelmand!</button>
                    @auth
                        <button class="open-modal image">
                            <span>
                                <i class="fas fa-camera"></i> <span>+</span>
                            </span>
                        </button>
                    @endauth
                </div>
            </section>
        </main>

        <x-footer/>
        <script src="{{ asset('js/show.js') }}"></script>
        <script src="{{ asset('js/headeralt.js') }}"></script>
        <script src="{{ asset('js/quantity.js') }}"></script>
        <script src="{{ asset('js/modalshoe.js') }}"></script>
        <script src="{{ asset('js/modal.js') }}"></script>

    </body>


</html>
