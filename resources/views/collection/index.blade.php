<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <style>
            body {
                font-family: 'Helvetica';
            }
        </style>
    </head>



    <body>
        <x-headeretc/>

        <div id="modal"></div>
        <div id="modal-container">
            <form action="{{ route('collection.store') }}" method="POST" enctype="multipart/form-data">

                @csrf
                <div class="form-group">
                    <label for="title">Titel</label>
                    <input class="form-shoes" type="text" name="title" id="title" placeholder="Titel">

                    @error('title')
                        <span class="error">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="price">Prijs</label>
                    <input class="form-shoes" type="text" name="price" id="price" placeholder="Prijs">
                    @error('price')
                    <span class="error">{{ $message }}</span>
                @enderror
                </div>
                <div class="form-group">
                    <label for="brand">Merk</label>
                    <select name="brand_id">
                        <option value="0">Kies merk</option>
                        @foreach ($brands as $brand )
                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                        @endforeach
                    </select>
                    @error('brand_id')
                        <span class="error">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="image">Afbeelding</label>
                    <input class="form-shoes" type="file" name="image" id="image">
                    @error('image')
                    <span class="error">{{ $message }}</span>
                @enderror
                </div>
                <button type="submit">Toevoegen</button>
            </form>
        </div>


        <main id="collection">
            <section>
                <div>
                    <img src="{{ asset('images/collection/collection1.jpg') }}" alt="">
                </div>
                <div>
                    <img src="{{ asset('images/collection/collection2.jpg') }}" alt="">
                </div>
                <div>
                    <img src="{{ asset('images/collection/collection3.jpg') }}" alt="">
                </div>
            </section>
            <section>
                <h2>
                    <span>Featured Items</span>
                </h2>
                @auth
                    <button class="open-modal">
                        <span>+</span>
                    </button>
                @endauth

                <div class="collection">

                    @foreach ($shoes as $shoe )

                    <a href="{{ route('details', ['shoe' => $shoe->id]) }}">
                        <div class="collection-card">
                            <div class="img-header">
                                <img src="/storage/{{ $shoe->image }}" alt="">
                            </div>
                            <div class="card-description">
                                <h3>{{ $shoe->title }}</h3>
                                <p>€{{ $shoe->price }}</p>
                            </div>
                        </div>
                    </a>

                    @endforeach

                </div>
            </section>
        </main>

        <x-footer/>
        <script src="{{ asset('js/headeralt.js') }}"></script>
        <script src="{{ asset('js/modal.js') }}"></script>

    </body>
</html>
