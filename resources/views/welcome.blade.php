<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <style>
            body {
                font-family: 'Helvetica';
            }
        </style>
    </head>
    <body>
        <x-header/>

        <main id="home">
            <section>
                <div class="teaser">
                    <div>
                        <h1>CHECK OUT THE WHOLE COLLECTION</h1>
                        <a href="{{ route('collection') }}">SEE COLLECTION</a>
                    </div>
                </div>
            </section>
            <section>
                <section>
                    <div class="inner-section">
                        <article>
                            <img src="{{ asset('images/shoes.svg') }}" alt="shoes">
                            <div>
                                <h2>Wat is Custom Calceus?</h2>
                                <p>Custom Calceus levert kwaliteit,
                                    persoonlijke, met de handgemaakte custom designs op voor schoenen.
                                    Met jou hulp maken wij voor je de beste custom schoenen
                                </p>
                            </div>
                        </article>
                        <article>
                            <img src="{{ asset('images/shoes2.svg') }}" alt="shoes">
                            <div>
                                <h2>Waarom Custom Calceus?</h2>
                                <p>Ben jij op zoek naar kwaliteitcustom schoenen.
                                     Dan is er helaas niemand anders beter dan ons.
                                     Zo is het nou eenmaal. No offence naar andere schoenenwinkels,
                                     maar het is gewoon zo. Betere kwaliteit, betere levering, betere prijzen.
                                     We kunnen door gaan.
                                </p>
                            </div>
                        </article>
                    </div>
                    <section>
                        <h2>
                            <span>Suggestions</span>
                        </h2>
                        <div class="front-row">
                            <div class="card">
                                <img src="{{ asset('images/suggestion.svg') }}" alt="">
                                <div class="card-description">
                                    <h3>Test name</h3>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{ asset('images/suggestion.svg') }}" alt="">
                                <div class="card-description">
                                    <h3>Test name</h3>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{ asset('images/suggestion.svg') }}" alt="">
                                <div class="card-description">
                                    <h3>Test name</h3>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{ asset('images/suggestion.svg') }}" alt="">
                                <div class="card-description">
                                    <h3>Test name</h3>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
            </section>
        </main>

        <x-footer/>
        <script src="{{ asset('js/header.js') }}"></script>

    </body>
</html>
